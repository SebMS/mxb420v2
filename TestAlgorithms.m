function[naieve,svm,glm] = TestAlgorithms()

XTrain = load('XTrain.mat');
XTrain = XTrain.XTrain;
[m,d] = size(XTrain);

XTest = load('XTest.mat');
XTest = XTest.XTest;

YTrain = load('YTrain.mat');
YTrain = YTrain.YTrain;

YTest = load('YTest.mat');
YTest = YTest.YTest;

model = fitcnb(XTrain,YTrain);
YPred = predict(model,XTest);
naieve = mean(YTest == YPred);

figure
confusionchart(YTest,YPred);
title('Naive Bayes Classifier');

model = fitcsvm(XTrain,YTrain);
YPred = predict(model,XTest);
svm = mean(YTest == YPred);
figure
confusionchart(YTest,YPred);
title('SVM');

if (d < m)
    model = fitglm(XTrain,YTrain, 'Distribution','binomial');

    glm = mean(YTest == predict(model,XTest));
else
    glm = 0;
end