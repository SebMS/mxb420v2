function TrainNewMLModel()

XTrain = load('XTrain.mat');
XTrain = XTrain.XTrain;
[m,d] = size(XTrain);

XTest = load('XTest.mat');
XTest = XTest.XTest;

YTrain = load('YTrain.mat');
YTrain = YTrain.YTrain;

YTest = load('YTest.mat');
YTest = YTest.YTest;

net = feedforwardnet(10);
[net,tr] = train(net,XTrain,YTrain);