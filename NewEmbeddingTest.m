
emb = fastTextWordEmbedding;

XTrain = load('LstmXTrain.mat');
XTrain = doc2cell(XTrain.tweets);
XTrain = embed(emb, XTrain);

XTest = load('LstmXTest.mat');
XTest = XTest.tweets;
XTest = embed(emb, XTest);

YTrain = load('YTrain.mat');
YTrain = YTrain.YTrain;

YTest = load('YTest.mat');
YTest = YTest.YTest;
