function WordEmbed()

data = load('CleanTweets.mat');

data = data.tweets;

data = tokenizedDocument(data);

embedding = trainWordEmbedding('CleanTweets.mat', 'Dimension', 100, ...
    'NumEpochs', 50, 'Verbose', 0);

save("wordEmbedding.mat", 'embedding');