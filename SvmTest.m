function SvmTest()

numTests = 15;
multiplier = 100;

cont = zeros(numTests,2);
dcont = zeros(numTests,1);
counter = 1;

for i = 1:numTests
    [n,d] = ProcessData(i*multiplier);
    dcont(counter) = d;
    features = FeatureSelect(300, 50);
    [naieve,svm,glm] = TestAlgorithms(features);
    cont(counter,1) = naieve;
    cont(counter,2) = svm;
    cont(counter,3) = glm;
    counter = counter + 1;
end

plot((1:numTests)*multiplier,cont(:,1), 'r');
hold on;
plot((1:numTests)*multiplier,cont(:,2), 'g');
plot((1:numTests)*multiplier,cont(:,3), 'b');
hold off;
figure;

plot(dcont,cont(:,1), 'r');
hold on
plot(dcont,cont(:,2), 'g');
plot(dcont,cont(:,3), 'b');

