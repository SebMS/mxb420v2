function idx = FeatureSelect(numfeat,neighbours);

XTrain = load('XTrain.mat');
XTrain = XTrain.XTrain';

YTrain = load('YTrain.mat');
YTrain = YTrain.YTrain;

[ranks, weights] = relieff(XTrain,YTrain,neighbours,'categoricalx','on');

idx = ranks <= numfeat;