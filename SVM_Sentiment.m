rng('default');
emb = fastTextWordEmbedding;
data = readLexicon;

%list positive words as test
idx = data.Label == 'Positive';
head(data(idx,:))

%train sentiment classifier
idx = ~isVocabularyWord(emb,data.Word);
data(idx,:) = [];

%set aside 10% of random words for testing
numWords = size(data,1);
cvp = cvpartition(numWords,'HoldOut',0.1);
dataTrain = data(training(cvp),:);
dataTest = data(test(cvp),:);

%convert the words in the training data to word vectors with word2vec
wordsTrain = dataTrain.Word;
XTrain = word2vec(emb,wordsTrain);
YTrain = dataTrain.Label;

%train the sentiment using SVM
mdl = fitcsvm(XTrain,YTrain);

%test classifier
wordsTest = dataTest.Word;
XTest = word2vec(emb,wordsTest);
YTest = dataTest.Label;

%prediction
[YPred,scores] = predict(mdl,XTest);

%visualise
figure
confusionchart(YTest,YPred);

%%%%%%%%%%%% Using our tweet data

%load our tweets
tweetContent = load("tweetcontent.mat");
tweetContent = tweetContent.tweetcontent;

% Randomly choose 5000 indices
s = RandStream('mt19937ar','Seed',25);
idxTest = randperm(s,1600000,1000);

tweetContent1000 = tweetContent(idxTest);
fprintf("loaded 5000 tweets")

%preprocess to remove bad things
documents = cleanTweets(tweetContent1000);


%remove words from the doc that do not appear in emb
idx = ~isVocabularyWord(emb,documents.Vocabulary);
documents = removeWords(documents,idx);

%visualise to see if the sentiment classifier generalises the tweets well
words = documents.Vocabulary;
words(ismember(words,wordsTrain)) = [];

vec = word2vec(emb,words);
[YPred,scores] = predict(mdl,vec);

figure
subplot(1,2,1)
idx = YPred == "Positive";
wordcloud(words(idx),scores(idx,1));
title("Predicted Positive Sentiment")

subplot(1,2,2)
wordcloud(words(~idx),scores(~idx,2));
title("Predicted Negative Sentiment")

% calculate mean sentiment score for a selection of the docs
idx = [7 34 331 1788 1820 1831 2185 21892 63734 76832 113276 120210];
for i = 1:numel(idx)
    words = string(documents(idx(i)));
    vec = word2vec(emb,words);
    [~,scores] = predict(mdl,vec);
    sentimentScore(i) = mean(scores(:,1));
end

%view predicted scores of those docs so that we can if we dun goofed
[sentimentScore' tweetContent5000(idx)] %commented out so I dont print 5k
%lines

%Use test data Y
testData = load('tweetpolarity.mat');
testData = testData.tweetpolarity;
%grab top 5000
testData5000 = testData(idxTest);
%cheese it so that it ranges from -2 to 2
testData5000 = testData5000 - 2;

% Compare the predicted sign (class) to see accuracy
mean(sign(testData5000) == sign(sentimentScore))
%testData5000 = ytest
%test classifier
wordsTest = dataTest.Word;
XTest = word2vec(emb,tweetContent5000);

%prediction
[YPred,scores] = predict(mdl,XTest);

testData5000 = categorical(testData5000);

%visualise
confusionchart(testData5000,YPred');

