function tweets = embed(emb, tweets)

% Get n of data
[numTweets, ~] = size(tweets);

for wordN = 1:numTweets
    words = string(tweets{wordN});
    idx = ~isVocabularyWord(emb,words);
    words(idx) = [];
    tweets{wordN} = word2vec(emb, words)';
end