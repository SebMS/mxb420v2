function pred = TestLstm(sentence);

sentence = cleanTweets(sentence);
load enc;

sentence = doc2sequence(enc,sentence,'UnknownWord','discard','Length',18);

load net;


pred = predict(net, sentence);

if round(pred(1),4) == single(0.3825)
    pred = "Error, no input words are in vocab";
end
end