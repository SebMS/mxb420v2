function tweets = cleanTweets(tweets)
% Cleans and pre-processes tweat data

% Clean
tweets = eraseURLs(tweets);
tweets = tokenizedDocument(tweets);
tweets = lower(tweets);
% Remove all twitter handles and some more urls
tweets = regexprep(tweets, "@\S*", "");
tweets = regexprep(tweets, "www\.\S*", "");
% More cleaning
tweets = erasePunctuation(tweets);
tweets = removeShortWords(tweets,1);
tweets = removeLongWords(tweets,15);
%tweets = normalizeWords(tweets);
tweets = removeStopWords(tweets);
tweets = removeEmptyDocuments(tweets);
