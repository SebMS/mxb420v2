function [X, idx, cutpoint] = CleanSeq(X)

[n,~] = size(X);

lens = zeros(n,1);
for i = 1:n
    X{i} = X{i}';
    [~, lens(i)] = size(X{i});
end

[values, idx] = sort(lens);

cutpoint = find(values,1,'first');