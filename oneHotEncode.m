function oneHot = oneHotEncode(tweetsCell, vocab)

% Get dimensionality and n of data
[~, d] = size(vocab);
[numTweets,~] = size(tweetsCell);

oneHot = zeros(d,numTweets);
for tweetN = 1:numTweets
    [~, loc] = ismember(tweetsCell{tweetN}, vocab); % Get one-hot locations
    loc = ind2vec(loc, d); % Convert to a vector
    oneHot(:,tweetN) = max(loc,[],2); % Combine the one-hot of each word 
end
