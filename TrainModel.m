function TrainModel()

XTrain = load('XLstmTrain.mat');
XTrain =  XTrain.XLstmTrain;
enc = wordEncoding(XTrain);
save enc;
histogram(doclength(XTrain));
XTrain = doc2sequence(enc,XTrain,'UnknownWord','discard','Length',18);
%XTrain = doc2sequence(emb, XTrain.tweets,'UnknownWord','discard', 'PaddingDirection','none');
YTrain = load('YTrainL.mat');
YTrain = YTrain.YTrainL;


XTest = load('XLstmTest.mat');
%XTest = doc2sequence(emb, XTest.tweets, 'PaddingDirection','none','UnknownWord','discard');
XTest = doc2sequence(enc,XTest.XLstmTest,'UnknownWord','discard','Length',18);
YTest = load('YTest.mat');
YTest = YTest.YTest;

YTrain = categorical(YTrain);
YTest = categorical(YTest);

% Model Params
inputSize = 1;
nWords = enc.NumWords;
embeddingDimension = 100;
lstmOutputSize = 100;
nClasses = 2;
maxEpochs = 2;
miniBatchSize = 20;

layers = [ ...
    sequenceInputLayer(inputSize)
    wordEmbeddingLayer(embeddingDimension, nWords)
    lstmLayer(lstmOutputSize,'OutputMode','last')
    fullyConnectedLayer(nClasses)
    softmaxLayer
    classificationLayer];

options = trainingOptions('sgdm', ...
    'ExecutionEnvironment','gpu', ...
    'GradientThreshold',1, ...
    'MaxEpochs',maxEpochs, ...
    'MiniBatchSize',miniBatchSize, ...
    'SequenceLength','longest', ...
    'Shuffle','never', ...
    'Verbose',0, ...
    'Plots','training-progress',...
    'InitialLearnRate', 0.01, ...
    'ValidationData', {XTest, YTest});


% Gmin = cellfun(@(XTrain) min(XTrain(:)), XTrain, 'Uniform', 0);
% Gmax = cellfun(@(XTrain) max(XTrain(:)), XTrain, 'Uniform', 0);
% XTrain = cellfun(@(M,minM,maxM) (M-minM) ./ (maxM-minM), XTrain, Gmin, Gmax, 'Uniform', 0);

net = trainNetwork(XTrain,YTrain,layers,options);

save net;
end






