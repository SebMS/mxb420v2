function [numTweets, d] = ProcessData()
% Stub for preproccessing method that reads data in and returns a cell 
% array of the one-hot encoding

% Total number of tweets that want to be used, max is 1.6m but will take
% forever to run if this large of a dataset is used

% Randomly get index of tweets, from a set seed for consistency
s = RandStream('mt19937ar','Seed',25);
idx = randperm(s,6918,6918);

% Load data in
dataIn = load('annotated.mat');
data = table2array(dataIn.annotated(:,2));  
% Subset data 
data = data(idx);

% Also save Y in better format with chosen random indices
Y = table2array(dataIn.annotated(:,1));
% Subset Y using same indices
Y = Y(idx);

tweets = cleanTweets(data);

% Get new size of dataset after empty tweets removed
[numTweets, ~] = size(tweets);

% embed words
emb = trainWordEmbedding(tweets);

% Convert back to cell array
tweetscell = doc2cell(tweets);

% embed and average for each tweet
X = zeros(numTweets,100);

for tweetN = 1:numTweets
    words = string(tweetscell{tweetN});
    idx = ~isVocabularyWord(emb,words);
    words(idx) = [];
    X(tweetN,:) = mean(word2vec(emb, words));
end

% Data saving 
% Partition 10% of the indices for testing, sometimes there will be 
% be overlap of 1 datum
idxTest = 1:round(numTweets/10);
idxTrain = round(numTweets/10):numTweets;

idxValL = round(numTweets/10):2*round(numTweets/10);
idxTrainL = 2*round(numTweets/10):numTweets;

% Save lstm data
XLstmTrain = tweets(idxTrainL);
save("XLstmTrain.mat", 'XLstmTrain', '-v7.3');

YTrainL = Y(idxTrainL);
save("YTrainL.mat", 'YTrainL', '-v7.3');

YValL = Y(idxValL);
save("YValL.mat", 'YValL', '-v7.3');

% Save one-hot testing for use in algorithms
XLstmTest = tweets(idxTest);
save("XLstmTest.mat", 'XLstmTest', '-v7.3');

% Save one-hot testing for use in algorithms
XLstmVal = tweets(idxValL);
save("XLstmVal.mat", 'XLstmVal', '-v7.3');

% Save one-hot training for use in algorithms
XTrain = X(idxTrain,:);
save("XTrain.mat", 'XTrain', '-v7.3');

% Save one-hot testing for use in algorithms
XTest = X(idxTest,:);
save("XTest.mat", 'XTest', '-v7.3');

% Save one-hot training for use in algorithms
YTrain = Y(idxTrain);
save("YTrain.mat", 'YTrain', '-v7.3');

% Save one-hot testing for use in algorithms
YTest = Y(idxTest);
save("YTest.mat", 'YTest', '-v7.3');

end